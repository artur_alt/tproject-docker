1. Клонируем проект с docker 
2. Для того чтобы не падал контейнер с elastic: "sudo sysctl -w vm.max_map_count=262144"
3. Запускаем контейнеры: "docker-compose up -d"
4. После старта контейнеров заходим в контейнер с postgres и создаем БД: "docker exec -it tproject_postgres_1 psql -U postgres -W postgres", "create database tproject;", "alter user postgres with encrypted password 'postgres';", "grant all privileges on database tproject to postgres;"
5. Клонируем в папку с docker основной проект: "git clone https://artur_alt@bitbucket.org/artur_alt/tproject.git"

